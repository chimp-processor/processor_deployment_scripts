
DELETE FROM ccprocessor.transaction WHERE card_id IN (10000001, 10000003, 10000005, 10000006, 10000007, 10000008, 10000009, 10000010, 10000011);
DELETE FROM ccprocessor.iso_transaction WHERE card_id IN (10000001, 10000003, 10000005, 10000006, 10000007, 10000008, 10000009, 10000010, 10000011);
DELETE FROM ccprocessor.balance WHERE id IN (10000001, 10000003, 10000005, 10000006, 10000007, 10000008, 10000009, 10000010, 10000011);
DELETE FROM ccprocessor.card WHERE id IN (10000001, 10000003, 10000005, 10000006, 10000007, 10000008, 10000009, 10000010, 10000011);
DELETE FROM ccprocessor.account WHERE id IN (10000001, 10000003, 10000005, 10000006, 10000007, 10000008, 10000009, 10000010, 10000011);
DELETE FROM ccprocessor.party WHERE id = 9999;
DELETE FROM ccprocessor.address WHERE id = 9999;
DELETE FROM ccprocessor.config_param;
DELETE FROM ccprocessor.program;
DELETE FROM ccprocessor.bin;

INSERT INTO ccprocessor.bin
(id,
 association,
 card_length,
 description,
 encrypt_algo,
 generated_key,
 number,
 pvki,
 track_format_id)
VALUES
  ('1', 'M', '16', 'desc', 'DES', NULL, '510842', '0', '1');

SET @PROGRAM_ID = 9999;
SET @PARTY_REF = '2628441525306452';

INSERT INTO ccprocessor.program (id, association, bin_extn, bin_extnyn, bin_id, code, expr_period, fld_activate, name, pin_gen_method )
  VALUES (@PROGRAM_ID, 'M', 510842, true, 1, 'PG', 12, '{"pinYN":true}', 'ProgramTxn', 'S');
INSERT INTO ccprocessor.address (id, city, country, home_zip, line1, line2, line3, shipping_city, shipping_line1, shipping_line2, shipping_line3, shipping_state, shipping_zip, state, zip)
  VALUES (9999, 'Los Angeles', 'USA',  null, '123 Sample Lane', '', '', null, null, null, null, null, null, 'CA', '90012');
INSERT INTO ccprocessor.party (id, admin, account_type, birth_date, create_date, email, email_confirm_date, email_uuid, email_yn, esign_consent, first_name, ip_address, last_name, last_ofac_run_date, main_phone, member_id, middle_name, mobile_phone, name, ofac_comments, office_phone, phone_yn, program_id, ref_id, reject_code, relation, sms_yn, ssn4, ssn_encrypt, time_zone, update_date, address_id)
  VALUES (9999, false, 'I', '1978-12-02', '2017-10-27 00:08:49', 'userone@yash.com', null, null, false, false, 'User', null, 'One', null, '1234567890', '123', null, '1234567890', null, null, '1234567890', false, @PROGRAM_ID, @PARTY_REF, null, null, false, '6781', 'h2c1fmlzGJimxy6c9Lom9w==', 'EDT', '2017-10-27 00:08:49', 9999);
INSERT INTO ccprocessor.config_param(id,json,record_type,program_id) VALUES (5,'{"ccAccountId":"1001","ccCardId":"1001","ddaLength":"15","ddaStartNum":"90515"}','pseudo',9999);
INSERT INTO ccprocessor.config_param (id, json, record_type, program_id) VALUES (6, '{"blkPinEnabled": true, "blkPinNumber": 3}', 'ruleparams', 9999);

# CARD 1

SET @CARD_1_ID = 10000001;

INSERT INTO ccprocessor.account (id, blocked_flag, created_date, dda_id, fee_charged_date, name, party_ref_id, ref_id, roundupyn, status, program_id)
  VALUES (@CARD_1_ID, false, '2018-04-29 00:00:00', '905151381451381', null, null, @PARTY_REF, '1001850032460061', false, 'A', @PROGRAM_ID);
INSERT INTO ccprocessor.balance (id, balance, purse_balance, currency, hold, rec_counter, account_id)
  VALUES (@CARD_1_ID, 100000000, 0, 'USD', 0, 0, @CARD_1_ID);
INSERT INTO ccprocessor.card (id, activation_date, active, blocked_by, blocked_flag, card_type, create_date, expiry_date, last_activated_card, masked_pan, pan4, pan_encrypt, party_ref_id, pin_encrypt, pin_updated_flag, ref_id, status, status_reason, svc_code_encrypt, update_date, wrong_pin_entries, account_id, program_id, card_fulfilment_batch_id, fulfillment_date_sent)
  VALUES (@CARD_1_ID, '2017-10-26 00:00:00', true, null, false, 'I', '2017-10-26 23:28:28', '2018-10-31', true, '510842XXXXXX3751', '3751', 'pG/MRDHxCbBDyFU75s/AiA==', @PARTY_REF, '1111FFFFFFFF', false, '1001980785227943', 'A', null, 'VZtEucns1809IRvMFJ4rUA==', null, 0, @CARD_1_ID, @PROGRAM_ID, 0, 0);

# CARD 3

SET @CARD_3_ID = 10000003;

INSERT INTO ccprocessor.account (id, blocked_flag, created_date, dda_id, fee_charged_date, name, party_ref_id, ref_id, roundupyn, status, program_id)
  VALUES (@CARD_3_ID, false, '2018-04-29 00:00:00','905151381451383', null, null, @PARTY_REF, '1001850032460063', false, 'A', @PROGRAM_ID);
INSERT INTO ccprocessor.balance (id, balance, purse_balance, currency, hold, rec_counter, account_id)
  VALUES (@CARD_3_ID, 2000, 0, 'USD', 0, 0, @CARD_3_ID);
INSERT INTO ccprocessor.card (id, activation_date, active, blocked_by, blocked_flag, card_fulfilment_batch_id, card_type, create_date, expiry_date, fulfillment_date_sent, fulfillment_file_name, last_activated_card, masked_pan, pan4, pan_encrypt, party_ref_id, pin_encrypt, pin_updated_flag, ref_id, status, status_reason, svc_code_encrypt, update_date, wrong_pin_entries, account_id, program_id)
  VALUES (@CARD_3_ID, '2017-12-09 00:00:00', true, null, false, 0, 'I', '2017-12-09 01:05:51', '2018-12-31', 0, null, true, '510842XXXXXX1490', '1490', 'EeXr5V6CFRiUGMTXHGzHcg==', @PARTY_REF, '1111FFFFFFFF', false, '10016871', 'A', null, '2gxsC/y4CX0INcFXq6dEQg==', null, 0, @CARD_3_ID, @PROGRAM_ID);

# CARD 5

SET @CARD_5_ID = 10000005;

INSERT INTO ccprocessor.account (id, blocked_flag, created_date, dda_id, fee_charged_date, name, party_ref_id, ref_id, roundupyn, status, program_id)
  VALUES (@CARD_5_ID, false, '2018-04-29 00:00:00','905151381451385', null, null, @PARTY_REF, '1001850032460065', false, 'A', @PROGRAM_ID);
INSERT INTO ccprocessor.balance (id, balance, purse_balance, currency, hold, rec_counter, account_id)
  VALUES (@CARD_5_ID, 100000000, 0, 'USD', 0, 0, @CARD_5_ID);
INSERT INTO ccprocessor.card (id, activation_date, active, blocked_by, blocked_flag, card_fulfilment_batch_id, card_type, create_date, expiry_date, fulfillment_date_sent, fulfillment_file_name, last_activated_card, masked_pan, pan4, pan_encrypt, party_ref_id, pin_encrypt, pin_updated_flag, ref_id, status, status_reason, svc_code_encrypt, update_date, wrong_pin_entries, account_id, program_id)
   VALUES (@CARD_5_ID, '2017-12-11 00:00:00', false, 'me', true, 0, 'I', '2017-12-11 19:27:25', '2018-12-31', 0, null, true, '510842XXXXXX6276', '6276', '287Dy4oKAkpnK4HXbmnP4Q==', @PARTY_REF, '1111FFFFFFFF', false, '10012086', 'B', 'Lost', 'MvtJJsU7sehT2Ql6GyclRw==', '2017-12-11 00:00:00', 0, @CARD_5_ID, @PROGRAM_ID);
# CARD 6

SET @CARD_6_ID = 10000006;

INSERT INTO ccprocessor.account (id, blocked_flag, created_date, dda_id, fee_charged_date, name, party_ref_id, ref_id, roundupyn, status, program_id)
  VALUES (@CARD_6_ID, false, '2018-04-29 00:00:00','905151381451386', null, null, @PARTY_REF, '1001850032460066', false, 'A', @PROGRAM_ID);
INSERT INTO ccprocessor.balance (id, balance, purse_balance, currency, hold, rec_counter, account_id)
  VALUES (@CARD_6_ID, 100000000, 0, 'USD', 0, 0, @CARD_6_ID);
INSERT INTO ccprocessor.card (id, activation_date, active, blocked_by, blocked_flag, card_fulfilment_batch_id, card_type, create_date, expiry_date, fulfillment_date_sent, fulfillment_file_name, last_activated_card, masked_pan, pan4, pan_encrypt, party_ref_id, pin_encrypt, pin_updated_flag, ref_id, status, status_reason, svc_code_encrypt, update_date, wrong_pin_entries, account_id, program_id)
  VALUES (@CARD_6_ID, '2017-12-11 00:00:00', true, null, false, 0, 'I', '2017-12-11 19:27:35', '2016-10-31', 0, null, true, '510842XXXXXX0808', '0808', '8wYzOTlFkf/967QT5tSN/w==', @PARTY_REF, '1111FFFFFFFF', false, '10018786', 'A', null, 'VZtEucns1809IRvMFJ4rUA==', null, 0, @CARD_6_ID, @PROGRAM_ID);

# CARD 7

SET @CARD_7_ID = 10000007;

INSERT INTO ccprocessor.account (id, blocked_flag, created_date, dda_id, fee_charged_date, name, party_ref_id, ref_id, roundupyn, status, program_id)
  VALUES (@CARD_7_ID, false, '2018-04-29 00:00:00','905151381451387', null, null, @PARTY_REF, '1001850032460067', false, 'A', @PROGRAM_ID);
INSERT INTO ccprocessor.balance (id, balance, purse_balance, currency, hold, rec_counter, account_id)
  VALUES (@CARD_7_ID, 100000000, 0, 'USD', 0, 0, @CARD_7_ID);
INSERT INTO ccprocessor.card (id, activation_date, active, blocked_by, blocked_flag, card_fulfilment_batch_id, card_type, create_date, expiry_date, fulfillment_date_sent, fulfillment_file_name, last_activated_card, masked_pan, pan4, pan_encrypt, party_ref_id, pin_encrypt, pin_updated_flag, ref_id, status, status_reason, svc_code_encrypt, update_date, wrong_pin_entries, account_id, program_id)
  VALUES (@CARD_7_ID, '2017-12-11 00:00:00', true, null, true, 0, 'I', '2017-12-11 19:27:46', '2018-12-31', 0, null, true, '510842XXXXXX9721', '9721', 'RidZ9htpXAaJQTSSstwcJQ==', @PARTY_REF, '1111FFFFFFFF', false, '10017374', 'B', 'Fraud', 'mQcWMqPf+tQABdOXyKXIlw==', null, 0, @CARD_7_ID, @PROGRAM_ID);

# CREATED ACCOUNT

SET @CARD_8_ID = 10000008;

INSERT INTO ccprocessor.account (id, blocked_flag, created_date, dda_id, fee_charged_date, name, party_ref_id, ref_id, roundupyn, status, program_id)
VALUES (@CARD_8_ID, false, '2018-04-29 00:00:00','905151381451388', null, null, @PARTY_REF, '1001850032460068', false, 'N', @PROGRAM_ID);
INSERT INTO ccprocessor.balance (id, balance, purse_balance, currency, hold, rec_counter, account_id)
VALUES (@CARD_8_ID, 100000000, 0, 'USD', 0, 0, @CARD_8_ID);
INSERT INTO ccprocessor.card (id, activation_date, active, blocked_by, blocked_flag, card_fulfilment_batch_id, card_type, create_date, expiry_date, fulfillment_date_sent, fulfillment_file_name, last_activated_card, masked_pan, pan4, pan_encrypt, party_ref_id, pin_encrypt, pin_updated_flag, ref_id, status, status_reason, svc_code_encrypt, update_date, wrong_pin_entries, account_id, program_id)
VALUES (@CARD_8_ID, '2017-12-11 00:00:00', true, null, true, 0, 'I', '2017-12-11 19:27:46', '2018-12-31', 0, null, true, '510842XXXXXX6561', '6561', 'RNzOeyNNoqUn418ioJP8Mg==', @PARTY_REF, '1111FFFFFFFF', false, '10017375', 'N', 'Fraud', '8vfNB6QYa6avv3qz9r/cXg==', null, 0, @CARD_8_ID, @PROGRAM_ID);

# BLOCKED ACCOUNT

SET @CARD_9_ID = 10000009;

INSERT INTO ccprocessor.account (id, blocked_flag, created_date, dda_id, fee_charged_date, name, party_ref_id, ref_id, roundupyn, status, program_id)
VALUES (@CARD_9_ID, false, '2018-04-29 00:00:00','905151381451389', null, null, @PARTY_REF, '1001850032460069', false, 'B', @PROGRAM_ID);
INSERT INTO ccprocessor.balance (id, balance, purse_balance, currency, hold, rec_counter, account_id)
VALUES (@CARD_9_ID, 100000000, 0, 'USD', 0, 0, @CARD_9_ID);
INSERT INTO ccprocessor.card (id, activation_date, active, blocked_by, blocked_flag, card_fulfilment_batch_id, card_type, create_date, expiry_date, fulfillment_date_sent, fulfillment_file_name, last_activated_card, masked_pan, pan4, pan_encrypt, party_ref_id, pin_encrypt, pin_updated_flag, ref_id, status, status_reason, svc_code_encrypt, update_date, wrong_pin_entries, account_id, program_id)
VALUES (@CARD_9_ID, '2017-12-11 00:00:00', true, null, true, 0, 'I', '2017-12-11 19:27:46', '2018-12-31', 0, null, true, '510842XXXXXX2690', '2690', 'lZsHkSv79IME65y4dpHObg==', @PARTY_REF, '1111FFFFFFFF', false, '10017374', 'B', 'Fraud', 'mQcWMqPf+tQABdOXyKXIlw==', null, 0, @CARD_9_ID, @PROGRAM_ID);

# PENDING_CLOSUE ACCOUNT

SET @CARD_10_ID = 10000010;

INSERT INTO ccprocessor.account (id, blocked_flag, created_date, dda_id, fee_charged_date, name, party_ref_id, ref_id, roundupyn, status, program_id)
VALUES (@CARD_10_ID, false, '2018-04-29 00:00:00','905151381451390', null, null, @PARTY_REF, '1001850032460070', false, 'P', @PROGRAM_ID);
INSERT INTO ccprocessor.balance (id, balance, purse_balance, currency, hold, rec_counter, account_id)
VALUES (@CARD_10_ID, 100000000, 0, 'USD', 0, 0, @CARD_10_ID);
INSERT INTO ccprocessor.card (id, activation_date, active, blocked_by, blocked_flag, card_fulfilment_batch_id, card_type, create_date, expiry_date, fulfillment_date_sent, fulfillment_file_name, last_activated_card, masked_pan, pan4, pan_encrypt, party_ref_id, pin_encrypt, pin_updated_flag, ref_id, status, status_reason, svc_code_encrypt, update_date, wrong_pin_entries, account_id, program_id)
VALUES (@CARD_10_ID, '2017-12-11 00:00:00', true, null, true, 0, 'I', '2017-12-11 19:27:46', '2018-12-31', 0, null, true, '510842XXXXXX9338', '9338', 'F5L+rEFYWuBOMKdXRilKGg==', @PARTY_REF, '1111FFFFFFFF', false, '10017374', 'A', 'Fraud', '2gxsC/y4CX0INcFXq6dEQg==', null, 0, @CARD_10_ID, @PROGRAM_ID);

# CLOSED ACCOUNT

SET @CARD_11_ID = 10000011;

INSERT INTO ccprocessor.account (id, blocked_flag, created_date, dda_id, fee_charged_date, name, party_ref_id, ref_id, roundupyn, status, program_id)
VALUES (@CARD_11_ID, false, '2018-04-29 00:00:00','905151381451391', null, null, @PARTY_REF, '1001850032460071', false, 'C', @PROGRAM_ID);
INSERT INTO ccprocessor.balance (id, balance, purse_balance, currency, hold, rec_counter, account_id)
VALUES (@CARD_11_ID, 100000000, 0, 'USD', 0, 0, @CARD_11_ID);
INSERT INTO ccprocessor.card (id, activation_date, active, blocked_by, blocked_flag, card_fulfilment_batch_id, card_type, create_date, expiry_date, fulfillment_date_sent, fulfillment_file_name, last_activated_card, masked_pan, pan4, pan_encrypt, party_ref_id, pin_encrypt, pin_updated_flag, ref_id, status, status_reason, svc_code_encrypt, update_date, wrong_pin_entries, account_id, program_id)
VALUES (@CARD_11_ID, '2017-12-11 00:00:00', true, null, true, 0, 'I', '2017-12-11 19:27:46', '2018-12-31', 0, null, true, '510842XXXXXX1134', '1134', 'MiXXHUSsgTeosyook4LiQw==', @PARTY_REF, '1111FFFFFFFF', false, '10017374', 'C', 'Fraud', '8vfNB6QYa6avv3qz9r/cXg==', null, 0, @CARD_11_ID, @PROGRAM_ID);


update ccprocessor.card set pin_encrypt = '7080FFFFFFFF',  pan_encrypt = 'D0725069786F2A036E9F3727F145A8758DA87FE19394F404481E7DF42DDBD02C',  masked_pan = '510842XXXXXX3757',  svc_code_encrypt = 'NrMdFHgBLrdGeps32WjmtQ==',  pan4 = '3757'  where id = 10000001;
update ccprocessor.card set pin_encrypt = '2816FFFFFFFF',  pan_encrypt = 'D0725069786F2A0311507F343465A1ECEA14DBE8E4CFFC2B481E7DF42DDBD02C',  masked_pan = '510842XXXXXX9988',  svc_code_encrypt = 'VZtEucns1809IRvMFJ4rUA==',  pan4 = '9988'  where id = 10000002;
update ccprocessor.card set pin_encrypt = '7683FFFFFFFF',  pan_encrypt = 'D0725069786F2A03B23EACE09DE20330CBEB5E04E34FEE7D481E7DF42DDBD02C',  masked_pan = '510842XXXXXX4455',  svc_code_encrypt = '8vfNB6QYa6avv3qz9r/cXg==',  pan4 = '4455'  where id = 10000004;
update ccprocessor.card set pin_encrypt = '7617FFFFFFFF',  pan_encrypt = 'D0725069786F2A0369A220E45B53EA5376A82740B0A6EA93481E7DF42DDBD02C',  masked_pan = '510842XXXXXX1490',  svc_code_encrypt = '2gxsC/y4CX0INcFXq6dEQg==',  pan4 = '1490'  where id = 10000003;
update ccprocessor.card set pin_encrypt = '5479FFFFFFFF',  pan_encrypt = 'D0725069786F2A03521A5C68C771BDF25D0FD893163858C0481E7DF42DDBD02C',  masked_pan = '510842XXXXXX6276',  svc_code_encrypt = 'MvtJJsU7sehT2Ql6GyclRw==',  pan4 = '6276'  where id = 10000005;
update ccprocessor.card set pin_encrypt = '8527FFFFFFFF',  pan_encrypt = 'D0725069786F2A032B0637F920CC89029A778F3E46677B61481E7DF42DDBD02C',  masked_pan = '510842XXXXXX0808',  svc_code_encrypt = 'VZtEucns1809IRvMFJ4rUA==',  pan4 = '0808'  where id = 10000006;
update ccprocessor.card set pin_encrypt = '3391FFFFFFFF',  pan_encrypt = 'D0725069786F2A032AFA8A704573A1981062EF1012C8DC46481E7DF42DDBD02C',  masked_pan = '510842XXXXXX9721',  svc_code_encrypt = 'mQcWMqPf+tQABdOXyKXIlw==',  pan4 = '9721'  where id = 10000007;

